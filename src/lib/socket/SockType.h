/*
 * SockType.h
 * Declares constants that reference socket types
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//no includes

//outer namespace declaration
namespace CPPSOCK {
	//typedef for socket type constants
	typedef int SockType;

	//inner namespace declaration
	namespace SOCK {
		//constant declarations
		extern const SockType STREAM; //TCP socket
		extern const SockType DGRAM; //UDP socket
		extern const SockType SEQPACKET; //sequenced packet socket
		extern const SockType RAW; //raw network protocol access
		extern const SockType RDM; //reliable UDP socket
	}
}

//end of header
