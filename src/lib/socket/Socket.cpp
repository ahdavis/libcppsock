/*
 * Socket.cpp
 * Implements a class that represents a network socket
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Socket.h"

//other includes
#include <sys/socket.h>
#include <unistd.h>
#include <cstring>
#include "../except/SockException.h"
#include "../except/CnxnException.h"
#include "../except/SendException.h"
#include "../except/RecvException.h"

//default constructor
CPPSOCK::Socket::Socket()
	: CPPSOCK::Socket(CPPSOCK::AF::INET, CPPSOCK::SOCK::STREAM)
{
	//no code needed
}

//main constructor
CPPSOCK::Socket::Socket(CPPSOCK::Domain newDomain, 
			CPPSOCK::SockType newType)
	: fd(-1), domain(newDomain), type(newType), hasCnxn(false)
{
	//init the file descriptor
	this->initFileDescriptor();
}

//destructor
CPPSOCK::Socket::~Socket() {
	this->close(); //close the Socket
}

//copy constructor
CPPSOCK::Socket::Socket(const CPPSOCK::Socket& s)
	: fd(-1), domain(s.domain), type(s.type), hasCnxn(false)
{
	//init the file descriptor
	this->initFileDescriptor();
}

//assignment operator
CPPSOCK::Socket& CPPSOCK::Socket::operator=(const CPPSOCK::Socket& src) {
	this->close(); //close the current connection
	this->domain = src.domain; //assign the domain field
	this->type = src.type; //assign the connection type
	this->initFileDescriptor(); //update the file descriptor
	return *this; //and return the instance
}

//getDomain method - returns the Socket's domain
CPPSOCK::Domain CPPSOCK::Socket::getDomain() const {
	return this->domain; //return the domain field
}

//getType method - returns the Socket's connection type
CPPSOCK::SockType CPPSOCK::Socket::getType() const {
	return this->type; //return the type field
}

//connected method - returns whether the Socket is connected
bool CPPSOCK::Socket::connected() const {
	return this->hasCnxn; //return the connection flag
}

//close method - closes the Socket
void CPPSOCK::Socket::close() {
	::close(this->fd); //close the socket
	this->hasCnxn = false; //and clear the connection flag
}

//connect method - connects the Socket to a remote address
void CPPSOCK::Socket::connect(const CPPSOCK::SockAddr& addr) {
	//attempt to connect to the remote address
	int res = ::connect(this->fd, (sockaddr*)&addr.data,
				sizeof(addr.data));

	//verify the connection
	if(res < 0) { //if the connection failed
		this->hasCnxn = false; //then clear the connection flag

		//and throw an exception
		throw CPPSOCK::CnxnException(addr.getPort(),
						addr.getStringIP());
	} else { //if the connection succeeded
		this->hasCnxn = true; //then set the connection flag
	}
}

//send method - sends a string of data to a remote host
void CPPSOCK::Socket::send(const std::string& data) {
	//attempt to send the data to the host
	int res = ::send(this->fd, data.c_str(), 
				std::strlen(data.c_str()), 0);

	//and make sure that the send succeeded
	if(res < 0) { //if the send failed
		//then throw an exception
		throw CPPSOCK::SendException(data);
	}
}

//receive method - receives a string of data from a remote host
std::string CPPSOCK::Socket::receive(std::size_t len) {
	//allocate a buffer to store the received data in
	char* buf = new char[len];

	//attempt to read into the buffer from the remote host
	int res = recv(this->fd, buf, len, 0);

	//make sure that the receive succeeded
	if(res < 0) { //if the receive failed
		//then deallocate the buffer
		delete[] buf;
		buf = nullptr;

		//and throw an exception
		throw CPPSOCK::RecvException();
	}

	//get the buffer as a string object
	std::string ret = buf;

	//deallocate the buffer
	delete[] buf;
	buf = nullptr;

	//and return the string object
	return ret;
}

//protected initFileDescriptor method - initializes the file descriptor
void CPPSOCK::Socket::initFileDescriptor() {
	//attempt to initialize the Socket
	this->fd = socket(this->domain, this->type, 0);

	//and make sure that the initialization succeeded
	if(this->fd == -1) { //if the initialization failed
		//then throw an exception
		throw CPPSOCK::SockException(this->domain, this->type);
	}
}

//end of implementation
