/*
 * Domain.cpp
 * Defines constants that reference communication domains
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Domain.h"

//include
#include <sys/socket.h>

//constant definitions
const CPPSOCK::Domain CPPSOCK::AF::UNIX = AF_UNIX;
const CPPSOCK::Domain CPPSOCK::AF::LOCAL = AF_LOCAL;
const CPPSOCK::Domain CPPSOCK::AF::INET = AF_INET;
const CPPSOCK::Domain CPPSOCK::AF::INET6 = AF_INET6;
const CPPSOCK::Domain CPPSOCK::AF::IPX = AF_IPX;
const CPPSOCK::Domain CPPSOCK::AF::NETLINK = AF_NETLINK;
const CPPSOCK::Domain CPPSOCK::AF::X25 = AF_X25;
const CPPSOCK::Domain CPPSOCK::AF::AX25 = AF_AX25;
const CPPSOCK::Domain CPPSOCK::AF::ATMPVC = AF_ATMPVC;
const CPPSOCK::Domain CPPSOCK::AF::APPLETALK = AF_APPLETALK;
const CPPSOCK::Domain CPPSOCK::AF::PACKET = AF_PACKET;
const CPPSOCK::Domain CPPSOCK::AF::ALG = AF_ALG;

//end of definitions
