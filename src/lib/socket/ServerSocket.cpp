/*
 * ServerSocket.cpp
 * Implements a class that represents a server socket
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ServerSocket.h"

//includes
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "../except/BindException.h"
#include "../except/AcceptException.h"
#include "../except/RecvException.h"

//default constructor
CPPSOCK::ServerSocket::ServerSocket()
	: CPPSOCK::Socket(), clientFD(-1)
{
	//no code needed
}

//main constructor
CPPSOCK::ServerSocket::ServerSocket(CPPSOCK::Domain newDomain,
					CPPSOCK::SockType newType)
	: CPPSOCK::Socket(newDomain, newType), clientFD(-1)
{
	//no code needed
}

//destructor
CPPSOCK::ServerSocket::~ServerSocket() {
	//no code needed
}

//copy constructor
CPPSOCK::ServerSocket::ServerSocket(const CPPSOCK::ServerSocket& ss)
	: CPPSOCK::Socket(ss), clientFD(ss.clientFD)
{
	//no code needed
}

//assignment operator
CPPSOCK::ServerSocket& CPPSOCK::ServerSocket::operator=(const 
						CPPSOCK::ServerSocket&
							src) {
	CPPSOCK::Socket::operator=(src); //assign the superclass data
	this->clientFD = src.clientFD; //assign the client file descriptor
	return *this; //and return the instance
}

//hasClientCnxn method - returns whether the server
//is connected to a client
bool CPPSOCK::ServerSocket::hasClientCnxn() const {
	//return whether the client file descriptor is valid
	return this->clientFD > 0; 
}

//bind method - binds the ServerSocket to a port
void CPPSOCK::ServerSocket::bind(std::uint16_t port) {
	//get a socket address object to bind to
	CPPSOCK::SockAddr addr(INADDR_ANY, this->domain, port);

	//attempt to bind to the socket address
	int res = ::bind(this->fd, (sockaddr*)&addr.data,
				sizeof(addr.data));

	//and verify that the bind succeeded
	if(res < 0) { //if the bind failed
		//then throw an exception
		throw CPPSOCK::BindException(port);
	}
}

//listen method - makes the ServerSocket start listening for
//incoming client connections
void CPPSOCK::ServerSocket::listen(int maxQueueSize) {
	::listen(this->fd, maxQueueSize); //listen for connections
}

//accept method - accepts a client connection
CPPSOCK::SockAddr CPPSOCK::ServerSocket::accept() {
	//get a SockAddr object to accept
	CPPSOCK::SockAddr client;

	//attempt to accept the client connection
	int sockSize = sizeof(sockaddr_in);
	this->clientFD = ::accept(this->fd, (sockaddr*)&client.data,
					(socklen_t*)&sockSize);

	//verify that the accept succeeded
	if(this->clientFD < 0) { //if the accept failed
		//then throw an exception
		throw CPPSOCK::AcceptException(client.getPort());
	}

	//and return the client data
	return client;
}

//overridden send method - sends a message to the client
void CPPSOCK::ServerSocket::send(const std::string& msg) {
	//send the message to the client
	if(this->hasClientCnxn()) { //if the socket is connected
		//then send the message to the client
		write(this->clientFD, msg.c_str(), 
				std::strlen(msg.c_str()));
	}
}

//overridden receive method - receives a message from the client
std::string CPPSOCK::ServerSocket::receive(std::size_t len) {
	//allocate a buffer to store the received data in
	char* buf = new char[len];

	//attempt to read into the buffer from the remote host
	int res = recv(this->clientFD, buf, len, 0);

	//make sure that the receive succeeded
	if(res < 0) { //if the receive failed
		//then deallocate the buffer
		delete[] buf;
		buf = nullptr;

		//and throw an exception
		throw CPPSOCK::RecvException();
	}

	//get the buffer as a string object
	std::string ret = buf;

	//deallocate the buffer
	delete[] buf;
	buf = nullptr;

	//and return the string object
	return ret;
}


//end of implementation
