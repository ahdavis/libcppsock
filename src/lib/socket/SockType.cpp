/*
 * SockType.cpp
 * Defines constants that reference socket types
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SockType.h"

//include
#include <sys/socket.h>

//constant definitions
const CPPSOCK::SockType CPPSOCK::SOCK::STREAM = SOCK_STREAM;
const CPPSOCK::SockType CPPSOCK::SOCK::DGRAM = SOCK_DGRAM;
const CPPSOCK::SockType CPPSOCK::SOCK::SEQPACKET = SOCK_SEQPACKET;
const CPPSOCK::SockType CPPSOCK::SOCK::RAW = SOCK_RAW;
const CPPSOCK::SockType CPPSOCK::SOCK::RDM = SOCK_RDM;

//end of definitions
