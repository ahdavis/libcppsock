/*
 * Domain.h
 * Declares constants that reference communication domains
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//no includes

//outer namespace declaration
namespace CPPSOCK {
	//typedef for domain constants
	typedef int Domain;

	//inner namespace declaration
	namespace AF {
		//constant declarations
		extern const Domain UNIX; //local UNIX communication
		extern const Domain LOCAL; //generic local communication
		extern const Domain INET; //IPv4 communication
		extern const Domain INET6; //IPv6 communication
		extern const Domain IPX; //IPX/Novell communication
		extern const Domain NETLINK; //kernel communication
		extern const Domain X25; //radio communication
		extern const Domain AX25; //amateur radio communication
		extern const Domain ATMPVC; //ATM communication
		extern const Domain APPLETALK; //AppleTalk communication
		extern const Domain PACKET; //low-level packet interface
		extern const Domain ALG; //kernel crypto interface
	}
}

//end of header
