/*
 * ServerSocket.h
 * Declares a class that represents a server socket
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdint>
#include "Socket.h"
#include "Domain.h"
#include "SockType.h"
#include "../addr/SockAddr.h"

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class ServerSocket final : public Socket 
	{
		//public fields and methods
		public:
			//default constructor
			ServerSocket();

			//main constructor
			ServerSocket(Domain newDomain, SockType newType);

			//destructor
			~ServerSocket();

			//copy constructor
			ServerSocket(const ServerSocket& ss);

			//assignment operator
			ServerSocket& operator=(const ServerSocket& src);

			//other methods
			
			//returns whether the ServerSocket is
			//connected to a client
			bool hasClientCnxn() const;
			
			//binds the ServerSocket to a port
			//throws a BindException if the bind fails
			void bind(std::uint16_t port);

			//makes the ServerSocket start listening for
			//incoming client connections
			void listen(int maxQueueSize);

			//accepts a client connection and returns
			//a SockAddr instance containing 
			//the connection data
			//throws an AcceptException if the accept fails
			SockAddr accept();

			//sends a message to the connected client
			void send(const std::string& msg) override;

			//retrieves a message from the connected client
			std::string receive(std::size_t len) override;

		//private fields and methods
		private:
			//field
			int clientFD; //the file descriptor for the client
	};
}

//end of header
