/*
 * Socket.h
 * Declares a class that represents a network socket
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdlib>
#include "Domain.h"
#include "SockType.h"
#include "../addr/SockAddr.h"

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class Socket {
		//public fields and methods
		public:
			//default constructor
			Socket();

			//main constructor
			Socket(Domain newDomain, SockType newType);

			//destructor
			virtual ~Socket();

			//copy constructor
			Socket(const Socket& s);

			//assignment operator
			Socket& operator=(const Socket& src);

			//getter methods
			
			//returns the Socket's domain
			Domain getDomain() const;

			//returns the Socket's type
			SockType getType() const;

			//returns whether the Socket is connected
			bool connected() const;

			//other methods
			
			//closes the Socket
			void close();

			//connects the Socket to a remote host
			//throws a CnxnException if the connection fails
			void connect(const SockAddr& addr);

			//sends a string of data to a remote host
			//throws a SendException if the send fails
			virtual void send(const std::string& data);

			//receives a string of data from a remote host
			//throws a RecvException if the receive fails
			virtual std::string receive(std::size_t len);

		//protected fields and methods
		protected:
			//method
			void initFileDescriptor(); //initializes the Socket

			//fields
			int fd; //the file descriptor for the Socket
			Domain domain; //the Socket's domain
			SockType type; //the Socket's connection type
			bool hasCnxn; //is the Socket connected?
	};
}

//end of header
