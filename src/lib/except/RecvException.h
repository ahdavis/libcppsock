/*
 * RecvException.h
 * Declares an exception that is thrown when a data receiving
 * operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <exception>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class RecvException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor is defaulted
			
			//destructor is defaulted
			
			//copy constructor is defaulted
			
			//assignment operator is defaulted
			
			//called when the exception is thrown
			const char* what() const throw() override;

		//no private fields or methods
	};
}

//end of header
