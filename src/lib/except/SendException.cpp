/*
 * SendException.cpp
 * Implements an exception that is thrown when 
 * a data sending operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SendException.h"

//include
#include <sstream>

//constructor
CPPSOCK::SendException::SendException(const std::string& badMsg)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to send ";
	ss << badMsg;
	ss << " to the remote host.";
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::SendException::~SendException() {
	//no code needed
}

//copy constructor
CPPSOCK::SendException::SendException(const CPPSOCK::SendException& se)
	: errMsg(se.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::SendException& CPPSOCK::SendException::operator=(const
						CPPSOCK::SendException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::SendException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
