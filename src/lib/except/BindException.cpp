/*
 * BindException.cpp
 * Implements an exception that is thrown when a server fails to
 * bind to a port
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "BindException.h"

//include
#include <sstream>

//constructor
CPPSOCK::BindException::BindException(std::uint16_t badPort)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to bind to port ";
	ss << badPort;
	ss << '.';
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::BindException::~BindException() {
	//no code needed
}

//copy constructor
CPPSOCK::BindException::BindException(const CPPSOCK::BindException& be)
	: errMsg(be.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::BindException& CPPSOCK::BindException::operator=(const
						CPPSOCK::BindException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::BindException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
