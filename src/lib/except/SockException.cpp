/*
 * SockException.cpp
 * Implements an exception that is thrown when socket creation fails
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SockException.h"

//include
#include <sstream>

//constructor
CPPSOCK::SockException::SockException(CPPSOCK::Domain badDomain,
					CPPSOCK::SockType badType)
	: errMsg()
{
	//init the error message
	std::stringstream ss;
	ss << "Failed to create socket! Domain: ";
	ss << badDomain;
	ss << " Type: ";
	ss << badType;
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::SockException::~SockException() {
	//no code needed
}

//copy constructor
CPPSOCK::SockException::SockException(const CPPSOCK::SockException& se)
	: errMsg(se.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::SockException& CPPSOCK::SockException::operator=(
				const CPPSOCK::SockException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::SockException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
