/*
 * HostException.cpp
 * Implements an exception that is thrown when a hostname retrieval
 * operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "HostException.h"

//include
#include <sstream>

//constructor
CPPSOCK::HostException::HostException(const std::string& badName)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to resolve hostname for domain ";
	ss << badName;
	ss << ".";
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::HostException::~HostException() {
	//no code needed
}

//copy constructor
CPPSOCK::HostException::HostException(const CPPSOCK::HostException& he)
	: errMsg(he.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::HostException& CPPSOCK::HostException::operator=(const
						CPPSOCK::HostException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::HostException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
