/*
 * SockException.h
 * Declares an exception that is thrown when socket creation fails
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include "../socket/Domain.h"
#include "../socket/SockType.h"

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class SockException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			SockException(Domain badDomain, SockType badType);

			//destructor
			~SockException();

			//copy constructor
			SockException(const SockException& se);

			//assignment operator
			SockException& operator=(const SockException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
