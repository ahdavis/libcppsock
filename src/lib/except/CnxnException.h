/*
 * CnxnException.h
 * Declares an exception that is thrown when a socket connection fails
 * Created on 7/24/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <cstdint>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class CnxnException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			CnxnException(std::uint16_t badPort,
					const std::string& badIP);

			//destructor
			~CnxnException();

			//copy constructor
			CnxnException(const CnxnException& ce);

			//assignment operator
			CnxnException& operator=(const CnxnException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
