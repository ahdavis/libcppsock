/*
 * RecvException.cpp
 * Implements an exception that is thrown when a data receiving
 * operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "RecvException.h"

//overridden what method - called when the exception is thrown
const char* CPPSOCK::RecvException::what() const throw() {
	return "Failed to receive data from remote host.";
}

//end of implementation
