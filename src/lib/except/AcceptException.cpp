/*
 * AcceptException.cpp
 * Implements an exception that is thrown when a server fails to accept
 * a client connection
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "AcceptException.h"

//include
#include <sstream>

//constructor
CPPSOCK::AcceptException::AcceptException(std::uint16_t badPort)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to accept a client connection on port ";
	ss << badPort;
	ss << '.';
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::AcceptException::~AcceptException() {
	//no code needed
}

//copy constructor
CPPSOCK::AcceptException::AcceptException(const CPPSOCK::AcceptException& 
						ae)
	: errMsg(ae.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::AcceptException& CPPSOCK::AcceptException::operator=(const
						CPPSOCK::AcceptException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::AcceptException::what() const throw() {

	return this->errMsg.c_str(); //return the error message
}

//end of implementation
