/*
 * SendException.h
 * Declares an exception that is thrown when a data sending operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class SendException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit SendException(const std::string& badMsg);

			//destructor
			~SendException();

			//copy constructor
			SendException(const SendException& se);

			//assignment operator
			SendException& operator=(const SendException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
