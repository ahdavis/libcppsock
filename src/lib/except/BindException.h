/*
 * BindException.h
 * Declares an exception that is thrown when a server fails to
 * bind to a port
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <cstdint>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class BindException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit BindException(std::uint16_t badPort);

			//destructor
			~BindException();

			//copy constructor
			BindException(const BindException& be);

			//assignment operator
			BindException& operator=(const BindException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
