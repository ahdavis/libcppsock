/*
 * CnxnException.cpp
 * Implements an exception that is thrown when a socket connection fails
 * Created on 7/24/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "CnxnException.h"

//include
#include <sstream>

//constructor
CPPSOCK::CnxnException::CnxnException(std::uint16_t badPort,
					const std::string& badIP)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to connect to ";
	ss << badIP;
	ss << " on port ";
	ss << badPort;
	ss << ".";
	this->errMsg = ss.str();
}

//destructor
CPPSOCK::CnxnException::~CnxnException() {
	//no code needed
}

//copy constructor
CPPSOCK::CnxnException::CnxnException(const CPPSOCK::CnxnException& ce)
	: errMsg(ce.errMsg)
{
	//no code needed
}

//assignment operator
CPPSOCK::CnxnException& CPPSOCK::CnxnException::operator=(const
						CPPSOCK::CnxnException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSOCK::CnxnException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
