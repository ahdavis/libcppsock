/*
 * AcceptException.h
 * Declares an exception that is thrown when a server fails to
 * accept a client connection
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <cstdint>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class AcceptException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit AcceptException(std::uint16_t badPort);

			//destructor
			~AcceptException();

			//copy constructor
			AcceptException(const AcceptException& ae);

			//assignment operator
			AcceptException& operator=(const AcceptException& 
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
