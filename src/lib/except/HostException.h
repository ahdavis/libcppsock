/*
 * HostException.h
 * Declares an exception that is thrown when a hostname retrieval
 * operation fails
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class HostException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit HostException(const std::string& badName);

			//destructor
			~HostException();

			//copy constructor
			HostException(const HostException& he);

			//assignment operator
			HostException& operator=(const HostException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
