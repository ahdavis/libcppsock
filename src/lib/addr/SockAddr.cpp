/*
 * SockAddr.cpp
 * Implements a class that represents a network address
 * Created on 7/24/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SockAddr.h"

//includes
#include <cstring>
#include <arpa/inet.h>

//constant initialization
const std::uint16_t CPPSOCK::SockAddr::DEFAULT_PORT = 80;

//default constructor
CPPSOCK::SockAddr::SockAddr()
	: CPPSOCK::SockAddr("127.0.0.1", CPPSOCK::AF::INET)
{
	//no code needed
}

//first constructor
CPPSOCK::SockAddr::SockAddr(const std::string& newIP,
				CPPSOCK::Domain newFamily)
	: CPPSOCK::SockAddr(newIP, newFamily, DEFAULT_PORT)
{
	//no code needed
}

//second constructor
CPPSOCK::SockAddr::SockAddr(std::uint64_t newIP,
				CPPSOCK::Domain newFamily)
	: CPPSOCK::SockAddr(newIP, newFamily, DEFAULT_PORT)
{
	//no code needed
}

//third constructor
CPPSOCK::SockAddr::SockAddr(const std::string& newIP,
				CPPSOCK::Domain newFamily,
				std::uint16_t newPort)
	: CPPSOCK::SockAddr(inet_addr(newIP.c_str()), newFamily, newPort)
{
	//no code needed
}

//fourth constructor
CPPSOCK::SockAddr::SockAddr(std::uint64_t newIP,
				CPPSOCK::Domain newFamily,
				std::uint16_t newPort)
	: ip(newIP), family(newFamily), port(newPort), data()
{
	this->initData(); //init the data field
}

//destructor
CPPSOCK::SockAddr::~SockAddr() {
	//no code needed
}

//copy constructor
CPPSOCK::SockAddr::SockAddr(const CPPSOCK::SockAddr& sa)
	: ip(sa.ip), family(sa.family), port(sa.port), data()
{
	this->initData(); //init the data field
}

//assignment operator
CPPSOCK::SockAddr& CPPSOCK::SockAddr::operator=(const CPPSOCK::SockAddr&
							src) {
	this->ip = src.ip; //assign the IP field
	this->family = src.family; //assign the family field
	this->port = src.port; //assign the port field
	this->initData(); //update the data field
	return *this; //and return the instance
}

//getNumericIP method - returns the IP as a number
std::uint64_t CPPSOCK::SockAddr::getNumericIP() const {
	return this->data.sin_addr.s_addr; //return the IP data
}

//getStringIP method - returns the IP as a string
std::string CPPSOCK::SockAddr::getStringIP() const {
	//get the address as a string
	std::string ret = inet_ntoa(this->data.sin_addr);

	//and return the address string
	return ret;
}

//getFamily method - returns the connection family
CPPSOCK::Domain CPPSOCK::SockAddr::getFamily() const {
	return this->family; //return the family field
}

//getPort method - returns the connection port
std::uint16_t CPPSOCK::SockAddr::getPort() const {
	return ntohs(this->data.sin_port); //return the port field
}

//first setIP method - sets the IP address from a string
void CPPSOCK::SockAddr::setIP(const std::string& newIP) {
	//call the other setIP method
	this->setIP(inet_addr(newIP.c_str()));
}

//second setIP method - sets the IP address from a number
void CPPSOCK::SockAddr::setIP(std::uint64_t newIP) {
	this->ip = newIP; //update the IP field
	this->data.sin_addr.s_addr = this->ip; //and update the data field
}

//setFamily method - sets the connection family
void CPPSOCK::SockAddr::setFamily(CPPSOCK::Domain newFamily) {
	this->family = newFamily; //update the family field
	this->data.sin_family = this->family; //and update the data field
}

//setPort method - sets the connection port
void CPPSOCK::SockAddr::setPort(std::uint16_t newPort) {
	this->port = newPort; //update the port field
	this->data.sin_port = htons(this->port); //update the data field
}

//protected initData method - initializes the data field
void CPPSOCK::SockAddr::initData() {
	//zero the data fields
	std::memset(&this->data, 0, sizeof(sockaddr_in));

	//update the data fields
	this->data.sin_addr.s_addr = this->ip;
	this->data.sin_family = this->family;
	this->data.sin_port = htons(this->port);
}

//end of implementation
