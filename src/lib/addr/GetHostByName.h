/*
 * GetHostByName.h
 * Declares a function that returns the IP of a host defined 
 * by its domain name
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace CPPSOCK {
	//function declaration
	std::string GetHostByName(const std::string& name);
}

//end of header
