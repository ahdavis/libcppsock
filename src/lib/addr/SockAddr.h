/*
 * SockAddr.h
 * Declares a class that represents a network address
 * Created on 7/24/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdint>
#include <netinet/in.h>
#include "../socket/Domain.h"

//namespace declaration
namespace CPPSOCK {
	//class declaration
	class SockAddr {
		//public fields and methods
		public:
			//default constructor
			SockAddr();

			//first constructor
			SockAddr(const std::string& newIP,
					Domain newFamily);

			//second constructor
			SockAddr(std::uint64_t newIP,
					Domain newFamily);

			//third constructor
			SockAddr(const std::string& newIP,
					Domain newFamily,
					std::uint16_t newPort);

			//fourth constructor
			SockAddr(std::uint64_t newIP,
					Domain newFamily,
					std::uint16_t newPort);

			//destructor
			virtual ~SockAddr();

			//copy constructor
			SockAddr(const SockAddr& sa);

			//assignment operator
			SockAddr& operator=(const SockAddr& src);

			//getter methods
			
			//returns the numeric IP address for the SockAddr
			std::uint64_t getNumericIP() const;

			//returns the IP address string for the SockAddr
			std::string getStringIP() const;

			//returns the connection family for the SockAddr
			Domain getFamily() const;

			//returns the port to connect on
			std::uint16_t getPort() const;

			//setter methods
			
			//sets the IP for the SockAddr
			void setIP(const std::string& newIP);
			void setIP(std::uint64_t newIP);

			//sets the connection family for the SockAddr
			void setFamily(Domain newFamily);

			//sets the connection port
			void setPort(std::uint16_t newPort);

		//protected fields and methods
		protected:
			//friendship declarations
			friend class Socket;
			friend class ServerSocket;

			//constant
			static const std::uint16_t DEFAULT_PORT;

			//method
			void initData(); //initializes the data field

			//fields
			std::uint64_t ip; //the SockAddr's IP address
			Domain family; //the connection family
			std::uint16_t port; //the connection port
			sockaddr_in data; //the address data
	};
}

//end of header
