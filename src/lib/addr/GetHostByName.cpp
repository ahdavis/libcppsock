/*
 * GetHostByName.cpp
 * Implements a function that returns the IP of a host defined 
 * by its domain name
 * Created on 7/25/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "GetHostByName.h"

//includes
#include <cstdlib>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "../except/HostException.h"

//GetHostByName function - returns the IP of a host 
//defined by its domain name
std::string CPPSOCK::GetHostByName(const std::string& name) {
	//get the database entry for the host
	hostent* he = NULL;
	he = gethostbyname(name.c_str());

	//make sure that the retrieval succeeded
	if(he == NULL) { //if the retrieval failed
		//then throw an exception
		throw CPPSOCK::HostException(name);
	}

	//get the address list from the host name
	in_addr** addr_list = (in_addr**)he->h_addr_list;

	//loop and assemble the IP string from the address list
	std::string ret;
	for(int i = 0; addr_list[i] != NULL; i++) {
		//append each address to the IP string
		ret += inet_ntoa(*addr_list[i]);
	}

	//and return the IP string
	return ret;
}

//end of implementation
