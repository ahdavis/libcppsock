/*
 * client.cpp
 * Main code file for the libcppsock client
 * Created on 7/23/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//includes
#include <cstdlib>
#include <iostream>
#include <string>
#include "../../lib/cppsock.h"

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//create a socket
	CPPSOCK::Socket sock;

	//get the IP for google.com
	std::string ip = CPPSOCK::GetHostByName("www.google.com");

	//define an address to connect to
	//the address object defaults to port 80
	CPPSOCK::SockAddr addr(ip, CPPSOCK::AF::INET);

	//attempt to connect the socket to the address
	try {
		sock.connect(addr); //connect the socket
	} catch(CPPSOCK::CnxnException& ce) {
		//handle the exception
		std::cerr << ce.what() << std::endl;
	}

	//utilize the connected socket
	if(sock.connected()) {
		//display a success message
		std::cout << "Connection to "
			<< addr.getStringIP()
			<< " on port "
			<< addr.getPort()
			<< " was successful!" << std::endl;

		//define a message to send through the socket
		std::string msg = "GET / HTTP/1.1\r\n\r\n";

		//attempt to send the message
		try {
			sock.send(msg); //send the message

			//print that the send succeeded
			std::cout << "Data sent!" << std::endl;

			//attempt to receive the result of the message
			try {
				//receive the data
				std::string res = sock.receive(2000);

				//print out that the receive succeeded
				std::cout << "Reply received" << std::endl;

				//and print out the received data
				std::cout << res << std::endl;

			} catch(CPPSOCK::RecvException& re) {
				//handle the exception
				std::cerr << re.what() << std::endl;
			}

		} catch(CPPSOCK::SendException& se) {
			//handle the exception
			std::cerr << se.what() << std::endl;
		}

		//and close the connection
		sock.close();
	}

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of header
