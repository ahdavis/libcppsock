/*
 * server.cpp
 * Main code file for the libcppsock demo server
 * Created on 7/29/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//includes
#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>
#include <utility>
#include <vector>
#include "../../lib/cppsock.h"

//function prototype
void connectionHandler(CPPSOCK::ServerSocket sock); 

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//create a vector of threads
	std::vector<std::thread> threads;

	//create a server socket
	CPPSOCK::ServerSocket ss;

	//bind the socket to port 8888
	try {
		ss.bind(8888);
	} catch(CPPSOCK::BindException& be) {
		//handle the exception
		std::cerr << be.what() << std::endl;
	}

	//start listening
	ss.listen(3);

	//display an info message
	std::cout << "Listening on port 8888" << std::endl;

	//loop and accept connections
	try {
		while(true) { //loop and accept connections
			//accept a connection
			ss.accept();

			//display that the connection was accepted
			std::cout << "Connection accepted" << std::endl;

			//send a message to the client
			ss.send("Hello Client\n");

			//start a thread to communicate with the client on
			std::thread cthread(connectionHandler, ss);

			//add the thread to the thread vector
			threads.push_back(std::move(cthread));

		}
	} catch(CPPSOCK::AcceptException& ae) {
		//handle the exception
		std::cerr << ae.what() << std::endl;
	}

	//join the threads
	for(auto& t : threads) {
		t.join();
	}

	//and return with no errors
	return EXIT_SUCCESS;
}

//connectionHandler function - communicates with the client
void connectionHandler(CPPSOCK::ServerSocket sock) {
	//send a message to the client using the thread
	sock.send("I am your connection handler.\n");
	sock.send("Type something and I will repeat it.\n");
	sock.send("Type 'q' to stop.\n");
	sock.send("\n");

	//loop and read data from the client
	while(true) {
		//send a prompt
		sock.send("> ");
		
		//receive the data from the client
		std::string msg = sock.receive(2000);

		//determine whether to exit the loop
		if(msg[0] == 'q') { //if a quit command was found
			//then display a goodbye message
			sock.send("Goodbye!\n");

			//and exit the loop
			break;
		}

		sock.send(msg); //repeat the message to the client
	}
}

//end of program
