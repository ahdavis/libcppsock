# Makefile for libcppsock
# Compiles the code for the library
# Created on 7/23/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the Lesser GNU General Public License version 3

# define the compiler
CXX=g++

# define the compiler flags for the library
CXXFLAGS=-fPIC -c -Wall -std=c++17

# define the compiler flags for the test client
tclient: CXXFLAGS=-c -Wall -std=c++17 -g

# define the compiler flags for the test server
tserver: CXXFLAGS=-c -Wall -std=c++17 -g

# define linker flags for the library
LDFLAGS=-shared 

# define linker flags for the test programs
TFLAGS=-lm -L./lib -Wl,-rpath,./lib -lcppsock -lpthread

# retrieve source code for the library
LEXCE=$(shell ls src/lib/except/*.cpp)
LSOCK=$(shell ls src/lib/socket/*.cpp)
LADDR=$(shell ls src/lib/addr/*.cpp)

# list the source code for the library
LSOURCES=$(LEXCE) $(LSOCK) $(LADDR)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.cpp=.o)

# retrieve source code for the test client
TCMAIN=$(shell ls src/test/client/*.cpp)

# list the source code for the test client
TCSOURCES=$(TCMAIN)

# compile the source code for the test program
TCOBJECTS=$(TCSOURCES:.cpp=.o)

# retrieve source code for the test server
TSMAIN=$(shell ls src/test/server/*.cpp)

# list the source code for the test server
TSSOURCES=$(TSMAIN)

# compile the source code for the test server
TSOBJECTS=$(TSSOURCES:.cpp=.o)

# define the name of the library
LIB=libcppsock.so

# define the name of the test client
TCLIENT=client

# define the name of the test server
TSERVER=server 

# rule for building both the library and the test programs
all: library tclient tserver 

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test client
tclient: $(TCSOURCES) $(TCLIENT)

# master rule for compiling the test server
tserver: $(TSSOURCES) $(TSERVER)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CXX) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/

# sub-rule for compiling the test client
$(TCLIENT): $(TCOBJECTS)
	$(CXX) $(TCOBJECTS) -o $@ $(TFLAGS)
	mkdir cbin
	mkdir cobj
	mv -f $(TCOBJECTS) cobj/
	mv -f $@ cbin/

# sub-rule for compiling the test server
$(TSERVER): $(TSOBJECTS)
	$(CXX) $(TSOBJECTS) -o $@ $(TFLAGS)
	mkdir sbin
	mkdir sobj
	mv -f $(TSOBJECTS) sobj/
	mv -f $@ sbin/ 

# rule for compiling source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libcppsock.so" ]; then \
		rm /usr/lib/libcppsock.so; \
	fi 
	if [ -d "/usr/include/cppsock" ]; then \
		rm -rf /usr/include/cppsock; \
	fi 
	
	mkdir /usr/include/cppsock
	mkdir /usr/include/cppsock/except 
	mkdir /usr/include/cppsock/socket 
	mkdir /usr/include/cppsock/addr
	
	cp src/lib/cppsock.h /usr/include/cppsock/
	cp $(shell ls src/lib/except/*.h) /usr/include/cppsock/except/
	cp $(shell ls src/lib/socket/*.h) /usr/include/cppsock/socket/
	cp $(shell ls src/lib/addr/*.h) /usr/include/cppsock/addr/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "sbin" ]; then \
		rm -rf sbin; \
	fi
	if [ -d "sobj" ]; then \
		rm -rf sobj; \
	fi
	if [ -d "cobj" ]; then \
		rm -rf cobj; \
	fi
	if [ -d "cbin" ]; then \
		rm -rf cbin; \
	fi 

# end of Makefile
